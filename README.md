# XFCE config
These are my setting and customisation files for the XFCE desktop

<br/>

## Table of contents <a name="TOC"></a>
- [Download](#Download)
- [Theme XFCE](#Theming)
- [Installation](#Installation)
- [Panel configuration](#Panel)
- [Whisker menu configuration](#Whisker)
- [Terminal configuration](#Terminal)
- [Thunar configuration](#Thunar)
- [Keyboard shortcuts](#Keyboard)
- [Clean-up](#Cleanup)
- [Licence and Status etc.](#End)


<br/><br/>


## [Downloading this repository](#TOC) <a name="Download"></a>
```
git clone https://gitlab.com/zyxomma1/xfce-config

```


<br/><br/>


## [Theme XFCE](#TOC) <a name="Theming"></a>
The following code will install [Matcha-gtk-theme](https://github.com/vinceliuice/Matcha-gtk-theme), [Papirus-icon-theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) and the Breeze icon theme if they are not already installed.
```
cd ~
cd xfce-config/icons/
chmod +x Themes 	#make shell script executable
./Themes 			#run script that Installs themes and icons I like
```


<br/><br/>


## [Installation of this repository](#TOC) <a name="Installation"></a>
### Move files to your computer
```
cd ~
cd xfce-config 	#cd into downloaded git repo
mv "catfish.xml"					"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/catfish.xml" 					#config for the file searcher catfish
mv "xfce4-appfinder.xml"			"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-appfinder.xml" 			#config for the appfinder
mv "xfce4-keyboard-shortcuts.xml"	"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml" 						#keybindings for applications
mv "xfce4-panel.xml"				"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml" 	#panel configuration
mv "xfwm4.xml"						"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml" 		#keybindings for the window manager
mv "xsettings.xml"					"/home/$USER/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml" 	#settings
sudo mv "lightdm-gtk-greeter.conf" "/etc/lightdm/lightdm-gtk-greeter.conf" 	#Lightdm greeter configuration

```


<br/><br/>


## [Panel configuration](#TOC) <a name="Panel"></a>
### Create panel
```
cd ~
cd xfce-config/panel/
while true; do
	read -p "Do you want 1 or 2 panels? " panels	#ask for amount of panels
	if [[ "$panels" == "1" ]]; then					#check if the choice is 1 or 2
		break
	elif [[ "$panels" == "2" ]]; then
		sed -i 's/declare -ar   XFCE_PANELS=(  '\''BOTTOM_PANEL'\''   )/declare -ar   XFCE_PANELS=(   '\''TOP_PANEL'\''   '\''BOTTOM_PANEL'\''   )/' bash_panel	#replace line in bash_panel
		break
	else
		echo "Invalid choice. Please enter '1' or '2'."
	fi
done
chmod +x bash_panel 	#make shell script executable
./bash_panel 			#run script that resets panel and configures it the way I like
```
### Fix audio and clipboard symbols in panel
```
cd ~
mkdir -p "/home/$USER/.config/gtk-3.0/" 	#make directories and any parents needed
mv "/home/$USER/xfce-config/panel/gtk.css" 	"/home/$USER/.config/gtk-3.0/gtk.css" 	#move css fix to directory
```
### Copy over customised icons for Brave and Firefox
```
sudo cp -a "/home/$USER/xfce-config/icons/". /usr/share/pixmaps/
```


<br/><br/>


## [Whisker menu configuration](#TOC) <a name="Whisker"></a>
Open the Whisker menu and add an application to favourites. This creates a configuration file. Run the code bellow that finds the config file and replaces it.
```
for file in /home/$USER/.config/xfce4/panel/whiskermenu-*.rc; do
  cp "$mountpoint/$USER/Backup/Computers/Configs/Linux Config/Git/XFCE-config/panel/whiskermenu-11.rc" "$file"
done
```


<br/><br/>


## [Terminal configuration](#TOC) <a name="Terminal"></a>
This copy's over the configuration files for the xfce terminal.
```
cp -a "/home/$USER/xfce-config/terminal/". "/home/$USER/.config/xfce4/terminal/"
```


<br/><br/>

## [Keyboard shortcuts](#TOC) <a name="Keyboard"></a>
Shortcuts that find/open different applications, print screen, logout, and control media can be installed via xfce4-keyboard-shortcuts. The shortcuts controlling the window manager are saved in xfwm4.



<br/><br/>


### [Clean-up](#TOC) <a name="Cleanup"></a>
```
cd ~
rm -rf  xfce-config			#delete the git folder
```


<br/><br/>


<a name="End"></a>
## Contributing 
No contributions will be accepted because this is how I like to setup my computer. Feel free to fork it if you want.

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.

## Project status
I just add stuff when it needs backed up.

## Authors and acknowledgement
0.  Me (Obviously)
